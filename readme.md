# CrossAtom's CrossWeb
#### Version 0.4.0 Beta

## Tags
- cw-row (instead div with "row" class). Attr: w (width), h (height), h-align, v-align
- cw-col (instead div with "col" class). Attr: w (width), h (height), h-align, v-align
- cw-text. Attr: font, size
